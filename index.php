<?php

    require_once("animal.php");
    require_once("buduk.php");
    require_once("kerasakti.php");
    $animal= new Animal("shoun");
    echo "Name Animal =". $animal -> name . "<br>";
    echo "Total Legs =". $animal -> legs . "<br>";
    echo "Cold Blooded =". $animal -> cold_blooded . "<br> <br>";
    
    $buduk= new Buduk("Buduk");
    echo "Name Animal =". $buduk -> name . "<br>";
    echo "Total Legs =". $buduk -> legs . "<br>";
    echo "Cold Blooded =". $buduk -> cold_blooded . "<br>";
    echo $buduk->jump() . "<br> <br>";

    $kerasakti= new Kerasakti("Kera Sakti");
    echo "Name Animal =". $kerasakti -> name . "<br>";
    echo "Total Legs =". $kerasakti -> legs . "<br>";
    echo "Cold Blooded =". $kerasakti -> cold_blooded . "<br>";
    echo $kerasakti->yell() . "<br> <br>";

?>